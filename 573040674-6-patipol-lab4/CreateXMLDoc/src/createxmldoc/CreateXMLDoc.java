package createxmldoc;

import java.io.IOException;
import javax.xml.parsers.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import static java.util.stream.DoubleStream.builder;
import javax.xml.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;

public class CreateXMLDoc {

    public static void main(String[] args) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try{
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();
        Element root = doc.createElement("profile");
        Element nameE = doc.createElement("name");
        Element major = doc.createElement("major");
        Element area = doc.createElement("area");
        Text nameT = doc.createTextNode("Patipol Thamdee");
        Text coe = doc.createTextNode("Computer Engineering");
        Text areaText = doc.createTextNode("Network");
        
        
        nameE.appendChild(nameT);
        root.appendChild(nameE);
        root.appendChild(major);
        major.appendChild(coe);
        major.appendChild(area);
        area.appendChild(areaText);
        doc.appendChild(root);
        
        OutputStream os = new FileOutputStream("myProfile.xml");
        
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer trans = tf.newTransformer();
        trans.transform(new DOMSource(doc),
                new StreamResult(os));
        

       
        
        }catch(Exception e){
            
        }
    }

}
