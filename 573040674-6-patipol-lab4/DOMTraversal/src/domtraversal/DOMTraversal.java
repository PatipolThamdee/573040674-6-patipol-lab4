package domtraversal;

import java.io.IOException;
import javax.xml.parsers.*;
import java.io.File;
import org.w3c.dom.*;

/**
 *
 * @author Zynnex
 */
public class DOMTraversal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {

            DocumentBuilder parser = factory.newDocumentBuilder();

            Document xmlDoc = parser.parse("nation.xml");

            Element rootElement = xmlDoc.getDocumentElement();
            
            followNode(rootElement);

        } catch (Exception e) {

        }
        

    }

    public static void followNode(Node node) throws IOException {
        writeNode(node);
        if (node.hasChildNodes()) {
            String name = node.getNodeName();
            int numChildren = node.getChildNodes().getLength();
            System.out.println("node " + name + " has " + numChildren
                    + " children");
            Node firstChild = node.getFirstChild();
            followNode(firstChild);
        }
        Node nextNode = node.getNextSibling();
        if (nextNode != null) {
            followNode(nextNode);
        }
    }
    
    public static void writeNode(Node node){
        System.out.print("Nodetype is ");
        if(node.getNodeType()==1){
            System.out.print("Element ");
        }
        else if (node.getNodeType()==3){
            System.out.print("Text ");
        }
        System.out.println("Name is " + node.getNodeName() + " value is " + node.getNodeValue() + " " );
    }
}
