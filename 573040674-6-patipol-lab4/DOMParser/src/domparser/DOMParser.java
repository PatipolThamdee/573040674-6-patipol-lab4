/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domparser;

import javax.xml.parsers.*;
import java.io.File;
import org.w3c.dom.*;

public class DOMParser {

    /**
     * @param args the command line arguments
     */
        
    public static void main(String[] args) {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {

            DocumentBuilder parser = factory.newDocumentBuilder();

            Document xmlDoc = parser.parse("nation.xml");

            Element rootElement = xmlDoc.getDocumentElement();
            
            System.out.println("The root Element name is " + rootElement.getNodeName() +"\n"+ "The root has attribute id=" + rootElement.getAttribute("id"));

        } catch (Exception e) {

        }
    }
}
